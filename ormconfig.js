const rootDir = process.env.NODE_ENV === "production" ? "dist" : "src";

const type = process.env.NODE_ENV === "production" ? "js" : "ts";

module.exports = {
  type: "postgres",
  host:
    process.env.NODE_ENV === "test"
      ? process.env.POSTGRES_HOST_TEST
      : process.env.POSTGRES_HOST,
  port:
    process.env.NODE_ENV === "test"
      ? process.env.POSTGRES_PORT_TEST
      : process.env.POSTGRES_PORT,
  username:
    process.env.NODE_ENV === "test"
      ? process.env.POSTGRES_USER_TEST
      : process.env.POSTGRES_USER,
  password:
    process.env.NODE_ENV === "test"
      ? process.env.POSTGRES_PASS_TEST
      : process.env.POSTGRES_PASS,
  database:
    process.env.NODE_ENV === "test"
      ? process.env.POSTGRES_NAME_TEST
      : process.env.POSTGRES_NAME,
  logging: false,
  entities: [`./${rootDir}/modules/**/infra/typeorm/entities/*.${type}`],
  migrations: [`./${rootDir}/shared/infra/typeorm/migrations/*.${type}`],
  cli: {
    migrationsDir: `./${rootDir}/shared/infra/typeorm/migrations`,
  },
};
