import { celebrate, Segments, Joi } from "celebrate";
import { Router } from "express";

import { CreateCustomerController } from "@modules/customers/useCases/createCustomer/CreateCustomerController";
import { DeleteCustomerController } from "@modules/customers/useCases/deleteCustomer/DeleteCustomerController";
import { ListCustomersController } from "@modules/customers/useCases/listCustomers/ListCustomersController";
import { ReadCustomerController } from "@modules/customers/useCases/readCustomer/ReadCustomerController";
import { UpdateCustomerController } from "@modules/customers/useCases/updateCustomer/UpdateCustomerController";
import { ensureAuthenticated } from "@shared/infra/http/middlewares/ensureAuthenticated";

const customersRoutes = Router();

const createCustomerController = new CreateCustomerController();
const updateCustomerController = new UpdateCustomerController();
const deleteCustomerController = new DeleteCustomerController();
const listCustomersController = new ListCustomersController();
const readCustomerController = new ReadCustomerController();

customersRoutes.post(
  "/",
  ensureAuthenticated,
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required().max(100),
      phone: Joi.string().required().length(14),
      email: Joi.string().email().required().max(250),
      birth_date: Joi.date().iso().required(),
      gender: Joi.string().required().length(1).valid("M", "F"),
      height: Joi.number().integer().positive().required().max(300),
      weight: Joi.number().positive().required().max(400),
    },
  }),
  createCustomerController.handle
);

customersRoutes.put(
  "/:customer_id",
  ensureAuthenticated,
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required().max(100),
      phone: Joi.string().required().length(14),
      email: Joi.string().email().required().max(250),
      birth_date: Joi.date().iso().required(),
      gender: Joi.string().required().length(1).valid("M", "F"),
      height: Joi.number().integer().positive().required().max(300),
      weight: Joi.number().positive().required().max(400),
    },
    [Segments.PARAMS]: {
      customer_id: Joi.string().uuid().required(),
    },
  }),
  updateCustomerController.handle
);

customersRoutes.get(
  "/",
  ensureAuthenticated,
  celebrate({
    [Segments.QUERY]: {
      offset: Joi.number().integer(),
      limit: Joi.number().integer(),
    },
  }),
  listCustomersController.handle
);

customersRoutes.delete(
  "/:customer_id",
  ensureAuthenticated,
  celebrate({
    [Segments.PARAMS]: {
      customer_id: Joi.string().uuid().required(),
    },
  }),
  deleteCustomerController.handle
);

customersRoutes.get(
  "/:customer_id",
  ensureAuthenticated,
  celebrate({
    [Segments.PARAMS]: {
      customer_id: Joi.string().uuid().required(),
    },
  }),
  readCustomerController.handle
);

export { customersRoutes };
