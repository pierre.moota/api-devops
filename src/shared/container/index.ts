import { container } from "tsyringe";

import "@shared/container/providers";

import { CustomersRepository } from "@modules/customers/infra/typeorm/repositories/CustomersRepository";
import { ICustomersRepository } from "@modules/customers/repositories/ICustomersRepository";
import { UsersRepository } from "@modules/users/infra/typeorm/repositories/UsersRepository";
import { UsersTokensRepository } from "@modules/users/infra/typeorm/repositories/UsersTokensRepository";
import { IUsersRepository } from "@modules/users/repositories/IUsersRepository";
import { IUsersTokensRepository } from "@modules/users/repositories/IUsersTokensRepository";

container.registerSingleton<IUsersRepository>(
  "UsersRepository",
  UsersRepository
);

container.registerSingleton<IUsersTokensRepository>(
  "UsersTokensRepository",
  UsersTokensRepository
);

container.registerSingleton<ICustomersRepository>(
  "CustomersRepository",
  CustomersRepository
);
