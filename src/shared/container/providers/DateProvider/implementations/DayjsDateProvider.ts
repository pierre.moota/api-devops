import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";

import { IDateProvider } from "../IDateProvider";

dayjs.extend(utc);
dayjs.extend(timezone);

class DayjsDateProvider implements IDateProvider {
  compareInHours(start_date: Date, end_date: Date): number {
    const endDateUtc = this.convertToUTC(end_date);
    const startDateUtc = this.convertToUTC(start_date);

    return dayjs(endDateUtc).diff(startDateUtc, "hours");
  }

  compareInMinutes(start_date: Date, end_date: Date): number {
    const endDateUtc = this.convertToUTC(end_date);
    const startDateUtc = this.convertToUTC(start_date);

    return dayjs(endDateUtc).diff(startDateUtc, "minutes");
  }

  convertToUTC(date: Date): string {
    return dayjs(date).utc().local().format();
  }

  currentDate(): Date {
    return dayjs().toDate();
  }

  compareInDays(start_date: Date, end_date: Date): number {
    const endDateUtc = this.convertToUTC(end_date);
    const startDateUtc = this.convertToUTC(start_date);

    return dayjs(endDateUtc).diff(startDateUtc, "days");
  }

  addDays(days: number): Date {
    return dayjs().add(days, "days").toDate();
  }

  addHours(hours: number): Date {
    return dayjs().add(hours, "hour").toDate();
  }

  compareIfBefore(start_date: Date, end_date: Date): boolean {
    return dayjs(start_date).isBefore(end_date);
  }

  addDaysToDate(date: Date, days: number): Date {
    return dayjs(date).utc(true).add(days, "days").toDate();
  }

  addMinutesToDate(date: Date, minutes: number): Date {
    return dayjs(date).utc(true).add(minutes, "minutes").toDate();
  }

  subtractMinutesToDate(date: Date, minutes: number): Date {
    return dayjs(date).utc(true).subtract(minutes, "minutes").toDate();
  }

  convertToTimezone(date: Date): Date {
    return dayjs(date).utc(true).toDate();
  }
}

export { DayjsDateProvider };
