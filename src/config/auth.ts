export default {
  secret_token: "50b727d114f7572bb52acde162d131b9",
  expires_in_token: "30m",
  secret_refresh_token: "09a32f80277ad21a5eb1483694899da1",
  expires_in_refresh_token: "30d",
  expires_refresh_token_days: 30,
};
