import { hash } from "bcrypt";
import { sign } from "jsonwebtoken";
import request from "supertest";
import { Connection } from "typeorm";
import { v4 as uuid } from "uuid";

import auth from "@config/auth";
import { app } from "@shared/infra/http/app";
import createConnection from "@shared/infra/typeorm";

let connection: Connection;
describe("Account User Controller", () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    const id = uuid();
    const password = await hash("123456", 8);

    await connection.query(
      `INSERT INTO users(id, name, email, password, created_at, updated_at, active)
        values('${id}', 'User Test', 'user@example.com', '${password}', 'now()', 'now()', true)`
    );
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it("should be able to read user's account", async () => {
    const responseAuth = await request(app).post("/auth/sessions").send({
      email: "user@example.com",
      password: "123456",
    });

    const { access_token } = responseAuth.body;

    const response = await request(app)
      .get("/users/account")
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty("id");
    expect(response.body).toHaveProperty("name");
  });

  it("should not be able to read an account to a non-existant user", async () => {
    const accessToken = sign(
      { email: "false@example.com" },
      auth.secret_token,
      {
        subject: uuid(),
        expiresIn: auth.expires_in_token,
      }
    );

    const response = await request(app)
      .get("/users/account")
      .set({
        Authorization: `Bearer ${accessToken}`,
      });

    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty("message");
    expect(response.body.message).toEqual("User not found");
  });
});
