interface IFindByUserDTO {
  user_id: string;
  refresh_token: string;
}

export { IFindByUserDTO };
