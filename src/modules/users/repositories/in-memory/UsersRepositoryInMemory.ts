import { ICreateUserDTO } from "@modules/users/dtos/ICreateUserDTO";
import { User } from "@modules/users/infra/typeorm/entities/User";

import { IUsersRepository } from "../IUsersRepository";

class UsersRepositoryInMemory implements IUsersRepository {
  users: User[] = [];

  async create({ name, email, password }: ICreateUserDTO): Promise<User> {
    const user = new User();

    Object.assign(user, {
      name,
      email,
      password,
    });

    this.users.push(user);

    return user;
  }

  async findByEmail(email: string): Promise<User> {
    const user = this.users.find(
      (userInMemory) => userInMemory.email === email
    );

    return user;
  }

  async findById(id: string): Promise<User> {
    const user = this.users.find((userInMemory) => userInMemory.id === id);

    return user;
  }
}

export { UsersRepositoryInMemory };
