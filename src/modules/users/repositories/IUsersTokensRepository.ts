import { ICreateUserTokenDTO } from "../dtos/ICreateUserTokenDTO";
import { IFindByUserDTO } from "../dtos/IFindByUserDTO";
import { UserToken } from "../infra/typeorm/entities/UserToken";

interface IUsersTokensRepository {
  create(data: ICreateUserTokenDTO): Promise<UserToken>;
  findByUserIdAndRefreshToken(data: IFindByUserDTO): Promise<UserToken>;
  deleteById(id: string): Promise<void>;
}

export { IUsersTokensRepository };
