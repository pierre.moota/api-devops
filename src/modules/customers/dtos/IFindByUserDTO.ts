interface IFindByUserDTO {
  user_id: string;
  offset?: number;
  limit?: number;
}

export { IFindByUserDTO };
