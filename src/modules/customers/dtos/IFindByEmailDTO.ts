interface IFindByEmailDTO {
  email: string;
  user_id: string;
}

export { IFindByEmailDTO };
