interface IFindByPhoneDTO {
  phone: string;
  user_id: string;
}

export { IFindByPhoneDTO };
