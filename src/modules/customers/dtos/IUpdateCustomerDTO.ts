interface IUpdateCustomerDTO {
  customer_id: string;
  user_id: string;
  name: string;
  phone: string;
  email: string;
  birth_date: string;
  gender: string;
  height: number;
  weight: number;
}

export { IUpdateCustomerDTO };
