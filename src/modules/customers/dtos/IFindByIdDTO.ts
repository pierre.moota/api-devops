interface IFindByIdDTO {
  customer_id: string;
  user_id: string;
  active?: boolean;
}

export { IFindByIdDTO };
