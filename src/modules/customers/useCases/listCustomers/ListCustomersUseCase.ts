import { inject, injectable } from "tsyringe";

import { IListCustomersDTO } from "@modules/customers/dtos/IListCustomersDTO";
import { Customer } from "@modules/customers/infra/typeorm/entities/Customer";
import { ICustomersRepository } from "@modules/customers/repositories/ICustomersRepository";
import { IUsersRepository } from "@modules/users/repositories/IUsersRepository";
import { AppError } from "@shared/errors/AppError";

@injectable()
class ListCustomersUseCase {
  constructor(
    @inject("CustomersRepository")
    private customersRepository: ICustomersRepository,
    @inject("UsersRepository")
    private usersRepository: IUsersRepository
  ) {}

  async execute({
    user_id,
    offset,
    limit,
  }: IListCustomersDTO): Promise<Customer[]> {
    const user = await this.usersRepository.findById(user_id);

    if (!user) {
      throw new AppError("User not found");
    }

    const customers = await this.customersRepository.findByUser({
      user_id,
      offset,
      limit,
    });

    return customers;
  }
}

export { ListCustomersUseCase };
