import { inject, injectable } from "tsyringe";

import { IDeleteCustomerDTO } from "@modules/customers/dtos/IDeleteCustomerDTO";
import { ICustomersRepository } from "@modules/customers/repositories/ICustomersRepository";
import { IUsersRepository } from "@modules/users/repositories/IUsersRepository";
import { AppError } from "@shared/errors/AppError";

@injectable()
class DeleteCustomerUseCase {
  constructor(
    @inject("CustomersRepository")
    private customersRepository: ICustomersRepository,
    @inject("UsersRepository")
    private usersRepository: IUsersRepository
  ) {}

  async execute({ customer_id, user_id }: IDeleteCustomerDTO): Promise<void> {
    const user = await this.usersRepository.findById(user_id);

    if (!user) {
      throw new AppError("User not found");
    }

    const customer = await this.customersRepository.findByIdAndUserId({
      customer_id,
      user_id,
      active: true,
    });

    if (!customer) {
      throw new AppError("Customer not found");
    }

    this.customersRepository.delete(customer_id);
  }
}

export { DeleteCustomerUseCase };
