import { v4 as uuidV4 } from "uuid";

import { Customer } from "@modules/customers/infra/typeorm/entities/Customer";
import { CustomersRepositoryInMemory } from "@modules/customers/repositories/in-memory/CustomersRepositoryInMemory";
import { UsersRepositoryInMemory } from "@modules/users/repositories/in-memory/UsersRepositoryInMemory";
import { CreateUserUseCase } from "@modules/users/useCases/createUser/CreateUserUseCase";
import { AppError } from "@shared/errors/AppError";

import { CreateCustomerUseCase } from "../createCustomer/CreateCustomerUseCase";
import { ReadCustomerUseCase } from "./ReadCustomerUseCase";

let customersRepositoryInMemory: CustomersRepositoryInMemory;
let usersRepositoryInMemory: UsersRepositoryInMemory;

let createUserUseCase: CreateUserUseCase;
let createCustomerUseCase: CreateCustomerUseCase;
let readCustomerUseCase: ReadCustomerUseCase;

let userId: string;
let customer: Customer;

describe("Read Customer", () => {
  beforeEach(async () => {
    customersRepositoryInMemory = new CustomersRepositoryInMemory();
    usersRepositoryInMemory = new UsersRepositoryInMemory();

    createUserUseCase = new CreateUserUseCase(usersRepositoryInMemory);
    createCustomerUseCase = new CreateCustomerUseCase(
      customersRepositoryInMemory,
      usersRepositoryInMemory
    );
    readCustomerUseCase = new ReadCustomerUseCase(
      customersRepositoryInMemory,
      usersRepositoryInMemory
    );

    const user = await createUserUseCase.execute({
      name: "User Test",
      email: "user@example.com",
      password: "123456",
    });

    userId = user.id;

    customer = await createCustomerUseCase.execute({
      user_id: userId,
      name: "Customer Test",
      phone: "+5548999669966",
      email: "customer@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });
  });

  it("should be able to read customers", async () => {
    const customerRead = await readCustomerUseCase.execute({
      user_id: customer.user_id,
      customer_id: customer.id,
    });

    expect(customerRead).toBeInstanceOf(Customer);
    expect(customerRead).toHaveProperty("id");
  });

  it("should not be able to read customers to a non-existent user", async () => {
    await expect(
      readCustomerUseCase.execute({
        user_id: uuidV4(),
        customer_id: customer.id,
      })
    ).rejects.toEqual(new AppError("User not found"));
  });

  it("should not be able to read a non-existent customer", async () => {
    await expect(
      readCustomerUseCase.execute({
        user_id: userId,
        customer_id: uuidV4(),
      })
    ).rejects.toEqual(new AppError("Customer not found"));
  });
});
