import { Request, Response } from "express";
import { container } from "tsyringe";

import { ReadCustomerUseCase } from "./ReadCustomerUseCase";

class ReadCustomerController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { customer_id } = request.params;
    const { id: user_id } = request.user;

    const readCustomerUseCase = container.resolve(ReadCustomerUseCase);

    const customer = await readCustomerUseCase.execute({
      customer_id,
      user_id,
    });

    return response.json(customer);
  }
}

export { ReadCustomerController };
