import { ICreateCustomerDTO } from "../dtos/ICreateCustomerDTO";
import { IFindByUserDTO } from "../dtos/IFindByUserDTO";
import { IFindByEmailDTO } from "../dtos/IFindByEmailDTO";
import { IFindByIdDTO } from "../dtos/IFindByIdDTO";
import { IFindByPhoneDTO } from "../dtos/IFindByPhoneDTO";
import { Customer } from "../infra/typeorm/entities/Customer";

interface ICustomersRepository {
  create(data: ICreateCustomerDTO): Promise<Customer>;
  findByUser(data: IFindByUserDTO): Promise<Customer[]>;
  findByIdAndUserId(data: IFindByIdDTO): Promise<Customer>;
  findByEmailAndUserId(data: IFindByEmailDTO): Promise<Customer>;
  findByPhoneAndUserId(data: IFindByPhoneDTO): Promise<Customer>;
  save(customer: Customer): Promise<void>;
  delete(customer_id: string): Promise<void>;
}

export { ICustomersRepository };
